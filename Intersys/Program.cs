﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Intersys
{
    class Program
    {
        static void Main(string[] args)
        {
            //zad4
            Console.WriteLine("Zadanie 4");
            int arrLength = int.Parse(Console.ReadLine());
            var arrInt = ReadNumbersLine();
            ReverseArray(arrInt);

            //zad5
            Console.WriteLine("Zadanie 5");
            string s = Console.ReadLine();
            CheckPalindrome(s);

            //zad6
            Console.WriteLine("Zadanie 6");
            int[] arr1 = ReadNumbersLine();
            int[] arr2 = ReadNumbersLine();
            CheckPermutation(arr1, arr2);

            //zad7
            Console.WriteLine("Zadanie 7");
            var numList = ReadNumbersLine();
            FindPowersOfTwo(numList);

            //zad8
            Console.WriteLine("Zadanie 8");
            int testCases = int.Parse(Console.ReadLine());
            var arr = new int[2];
            for (int i = 0; i < testCases; i++)
            {
                arr = ReadNumbersLine();
                FindPrimes(arr[0], arr[1]);
            }

            //zad11
            Console.WriteLine("Zadanien 11");
            int testNumbers = int.Parse(Console.ReadLine());

            var numbers = Regex.Replace(Console.ReadLine(), "[^0-9]", "").ToCharArray();
            FindMostCommon(numbers);

            //zad12
            Console.WriteLine("Zadanie 12");
            int N = int.Parse(Console.ReadLine());
            var digArr = ReadNumbersLine();
            DigitSum(digArr);
        }

        private static void FindPowersOfTwo(int[] numList)
        {
            Array.Sort(numList);
            int biggestPowerOfTwo = 0;

            for (int i = numList.Length - 1; i > 0; i--)
            {
                if ((numList[i] & (numList[i] - 1)) == 0)
                {
                    biggestPowerOfTwo = numList[i];
                    break;
                }
            }
            if (biggestPowerOfTwo == 0)
            {
                Console.WriteLine("NA");
                return;
            }

            var strBuilder = new StringBuilder();
            while (biggestPowerOfTwo > 0)
            {
                strBuilder.Append($"{biggestPowerOfTwo},");
                biggestPowerOfTwo /= 2;
            }

            strBuilder.Length--;
            var charArr = strBuilder.ToString().ToCharArray();
            Array.Reverse(charArr);
            Console.WriteLine(new string(charArr));
        }

        private static void DigitSum(int[] digArr)
        {
            int maxSum = int.MinValue;
            int positionInArray = -1;
            int tmpSum;

            for (int i = 0; i < digArr.Length; i++)
            {
                tmpSum = CheckDigitSum(digArr[i]);

                if (tmpSum > maxSum)
                {
                    maxSum = tmpSum;
                    positionInArray = i;
                }
            }

            Console.WriteLine(positionInArray);
        }

        private static int CheckDigitSum(int dig)
        {
            int digSum = 0;
            for (int n = dig; n > 0; digSum += n % 10, n /= 10) ;

            return digSum;
        }

        private static void FindMostCommon(char[] numbers)
        {
            var frequencyArray = numbers.GroupBy(x => x).OrderByDescending(o => o.Count());
            var mostCommonNum = new Number();

            foreach (var item in frequencyArray)
            {
                if (item.Count() >= mostCommonNum.Frequency)
                {
                    mostCommonNum.Value = int.Parse(char.GetNumericValue(item.Key).ToString());
                    mostCommonNum.Frequency = item.Count();
                }
                else
                    break;
            }
            Console.WriteLine((mostCommonNum.Value));
        }

        private static void FindPrimes(int m, int n)
        {
            int counter = 0;


            if (m == 1 && m != n && n != 2)
                m++;

            for (int i = m; i < n; i++)
            {
                bool isPrime = true;
                for (int j = i - 1; j > 1; j--)
                {
                    if (i % j == 0)
                        isPrime = false;
                }
                if (isPrime)
                    counter++;
            }

            Console.WriteLine(counter);
        }

        private static void CheckPermutation(int[] arr1, int[] arr2)
        {
            var dictionary = new Dictionary<int, int>();
            bool isPermutation = true;
            foreach (var item in arr1)
            {
                int count;
                dictionary.TryGetValue(item, out count);
                dictionary[item] = count + 1;
            }

            foreach (var item in arr2)
            {
                int count;
                if (!dictionary.TryGetValue(item, out count) || count == 0)
                {
                    isPermutation = false;
                    break;
                }
                dictionary[item] = count - 1;
            }
            Console.WriteLine(isPermutation ? "YES" : "NO");

        }

        private static void CheckPalindrome(string s)
        {
            string onlyLetters = Regex.Replace(s, "[^a-zA-Z]", "");
            Console.WriteLine(onlyLetters.SequenceEqual(onlyLetters.Reverse()) ? "YES" : "NO");
        }

        private static void ReverseArray(int[] arrInt)
        {
            Array.Reverse(arrInt);
            foreach (var item in arrInt)
            {
                Console.Write(item + " ");
            }
            Console.WriteLine();
        }

        private static int[] ReadNumbersLine()
        {
            return Array.ConvertAll(Regex.Split(Console.ReadLine(), @"\D+"), int.Parse);
        }
    }

    public class Number
    {
        public int Frequency { get; set; }
        public int Value { get; set; }
    }
}
